/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.util;

import java.io.*;
/**
 * File related utilities methods
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: FileUtils.java,v 1.3 2004/09/19 20:07:04 thlee Exp $ $Name:  $
 */
public final class FileUtils
{

    /**
     * {@inheritDoc}
     */
    private FileUtils()
    {
    };

    /** the forward slash * */
    private static final String FSLASH = "/";

    /** the back slash * */
    private static final String BSLASH = "\\";

    /** the separator * */
    private static final String SEPARATOR = FSLASH; //use forward slash since

    // both unix and windows jdk
    // support it
    /**
     * Concat the strings to be a valid file path 
     * @param args the string
     * @return the file path
     * @todo add doc to describe the behavior
     */
    public static String concatFilePath(String[] args)
    {
        StringBuffer sb = new StringBuffer(args[0]);
        for (int i = 1; i < args.length; i++)
        {
            concatFilePath(sb, args[i]);
        }
        return sb.toString();
    }

    //TODO: Describe behaviors
    /**
     * Concat filepath with the prefix and suffix
     * @param prefix the prefix
     * @param suffix the suffix
     * @return the file path
     * @todo add doc to describe the behavior
     */
    public static String concatFilePath(String prefix, String suffix)
    {
        return concatFilePath(new StringBuffer(prefix), suffix);
    }

    /**
     * concat a meaning file path
     * @param prefix the prefix
     * @param suffix the suffix
     * @return the concat'ed file path
     */
    private static String concatFilePath(StringBuffer prefix, String suffix)
    {
        String pf = prefix.toString();
        if (pf.endsWith(FSLASH) || pf.endsWith(BSLASH))
        {
            if (suffix.startsWith(FSLASH) || suffix.startsWith(BSLASH))
            {
                if (suffix.length() > 1)
                {
                    prefix.append(suffix.substring(1));
                }
                //else do nothing
            } else
            {
                prefix.append(suffix);
            }
        } else
        {
            if (suffix.startsWith(FSLASH) || suffix.startsWith(BSLASH))
            {
                prefix.append(suffix);
            } else
            {
                prefix.append(FSLASH);
                prefix.append(suffix);
            }
        }
        return prefix.toString();
    }

    /**
     * Copy the input to the output. Will not close the output after copying
     * @param input the input stream
     * @param output the output
     * @param chunkSize the chunk size
     * 
     */
    public static void copyStream(InputStream input, OutputStream output, int chunkSize) throws IOException
    {
        byte[] buffer = new byte[chunkSize];
        int n = input.read(buffer, 0, chunkSize);
        while (n != -1)
        {
            output.write(buffer, 0, n);
            n = input.read(buffer, 0, chunkSize);
        }
        output.flush();
    }
}