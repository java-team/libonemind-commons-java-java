/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.util;

import java.io.*;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
/**
 * A map utility class
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: MapUtils.java,v 1.3 2004/10/31 16:02:21 thlee Exp $ $Name:  $
 */
public final class MapUtils
{

    /**
     * {@inheritDoc}
     */
    private MapUtils()
    {
    };

    /**
     * Dump the map in name=value lines
     * @param m the map
     * @param writer the writer
     * @throws IOException if there's IO problem
     */
    public static void dump(Map m, Writer writer) throws IOException
    {
        Iterator it = m.keySet().iterator();
        while (it.hasNext())
        {
            String key = (String) it.next();
            writer.write(key + " = " + m.get(key) + "\n");
        }
    }

    /**
     * Get a string representation of the content in the map
     * @param m the map
     * @return a string with the format of a=b separated by line break
     * @throws IOException
     */
    public static String toString(Map m) throws IOException
    {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(bout);
        dump(m, writer);
        writer.flush();
        bout.close();
        return new String(bout.toByteArray());        
    }
}