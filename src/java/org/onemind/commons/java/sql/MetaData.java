/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */
package org.onemind.commons.java.sql;

import java.util.*;
/**
 * A MetaData contains metadata about a database table
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: MetaData.java,v 1.2 2004/08/26 12:33:18 thlee Exp $ $Name:  $
 */
public class MetaData
{

    /** the fields * */
    private Map _fields = new LinkedHashMap();

    /** the name * */
    private String _name;

    /** the primary key field name * */
    private String _idFieldName;

    /**
     * create a MetaData with id <i>id </i>
     * @param name the name
     */
    public MetaData(String name)
    {
        this._name = name;
    }

    /**
     * create a MetaData with id <i>id </i> and primaryke <i>pj </i>
     * @param name the name
     * @param idFieldName the unique identifier field name
     */
    public MetaData(String name, String idFieldName)
    {
        this(name);
        _idFieldName = idFieldName;
    }

    /**
     * add a new field
     * @param field the field
     */
    public void addField(Field field)
    {
        _fields.put(field.getName(), field);
    }

    /**
     * get the field with id <i>name </i>
     * @param name the name
     * @return the field, or null
     */
    public Field getField(String name)
    {
        return (Field) _fields.get(name);
    }

    /**
     * get the fields in this MetaData
     * @return Map the map
     */
    public Map getFields()
    {
        return Collections.unmodifiableMap(_fields);
    }

    /**
     * return the id of the MetaData
     * @return the id
     */
    public String getId()
    {
        return _name;
    }

    /**
     * return the primary key field
     * @return the id field
     */
    public Field getIdField()
    {
        return (Field) _fields.get(getIdField());
    }

    /**
     * Return the id field name
     * @return the id field name
     */
    public String getIdFieldName()
    {
        return _idFieldName;
    }

    /**
     * return whether there's a field with id <i>name </i>
     * @param name the field name
     * @return true if has the field
     */
    public boolean hasField(String name)
    {
        return _fields.containsKey(name);
    }

    /**
     * set the fields of the MetaData
     * @param fields the fields
     */
    public void setFields(Map fields)
    {
        _fields.clear();
        _fields.putAll(fields);
    }

    /**
     * Set the id field name
     * @param string the id field name
     */
    public void setIdFieldName(String string)
    {
        _idFieldName = string;
    }
}