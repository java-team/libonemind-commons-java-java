/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.sql;

/**
 * A Field contains meta infomration of a field in the database table
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: Field.java,v 1.2 2004/08/26 12:33:18 thlee Exp $ $Name:  $
 */
public class Field
{

    /** the name * */
    private String _name;

    /** the sql type * */
    private int _type;

    /**
     * create a new field with name <i>name </i> and type <i>type </i>
     * @param name the name
     * @param type the type
     */
    public Field(String name, int type)
    {
        _name = name;
        _type = type;
    }

    /**
     * return the name of the field
     * @return the name
     */
    public String getName()
    {
        return _name;
    }

    /**
     * get the type of the field
     * @return the type
     */
    public int getType()
    {
        return _type;
    }

    /**
     * {@inheritDoc}
     */
    public String toString()
    {
        return "Field:" + _name + ":" + _type;
    }
}