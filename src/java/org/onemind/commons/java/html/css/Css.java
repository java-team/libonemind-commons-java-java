/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.html.css;

import java.util.*;
import org.onemind.commons.java.lang.Enum;
/**
 * Contains css related method and constants
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: Css.java,v 1.3 2005/03/07 17:28:17 thlee Exp $ $Name:  $
 */
public class Css
{

    /**
     * Represent the attribute (Currently contains only visual attr)
     * @author TiongHiang Lee (thlee@onemindsoft.org)
     * @version $Id: Css.java,v 1.3 2005/03/07 17:28:17 thlee Exp $ $Name:  $
     */
    public static final class Attr extends Enum
    {

        private static Map _cssAttrs = new HashMap();

        public static final Attr background = new Attr("background");

        public static final Attr background_attachment = new Attr("background-attachment");

        public static final Attr background_color = new Attr("background-color");

        public static final Attr background_image = new Attr("background-image");

        public static final Attr background_position = new Attr("background-position");

        public static final Attr background_repeat = new Attr("background-repeat");

        public static final Attr border = new Attr("border");

        public static final Attr border_bottom = new Attr("border-bottom");

        public static final Attr border_bottom_color = new Attr("border-bottom-color");

        public static final Attr border_bottom_style = new Attr("border-bottom-style");

        public static final Attr border_bottom_width = new Attr("border-bottom-width");

        public static final Attr border_collapse = new Attr("border-collapse");

        public static final Attr border_color = new Attr("border-color");

        public static final Attr border_left = new Attr("border-left");

        public static final Attr border_left_color = new Attr("border-left-color");

        public static final Attr border_left_style = new Attr("border-left-style");

        public static final Attr border_left_width = new Attr("border-left-width");

        public static final Attr border_right = new Attr("border-right");

        public static final Attr border_right_color = new Attr("border-right-color");

        public static final Attr border_right_style = new Attr("border-right-style");

        public static final Attr border_right_width = new Attr("border-right-width");

        public static final Attr border_spacing = new Attr("border-spacing");

        public static final Attr border_style = new Attr("border-style");

        public static final Attr border_top = new Attr("border-top");

        public static final Attr border_top_color = new Attr("border-top-color");

        public static final Attr border_top_style = new Attr("border-top-style");

        public static final Attr border_top_width = new Attr("border-top-width");

        public static final Attr border_width = new Attr("border-width");

        public static final Attr bottom = new Attr("bottom");

        public static final Attr caption_side = new Attr("caption-side");

        public static final Attr clear = new Attr("clear");

        public static final Attr clip = new Attr("clip");

        public static final Attr color = new Attr("color");

        public static final Attr content = new Attr("content");

        public static final Attr counter_increment = new Attr("counter-increment");

        public static final Attr counter_reset = new Attr("counter-reset");

        public static final Attr cursor = new Attr("cursor");

        public static final Attr direction = new Attr("direction");

        public static final Attr display = new Attr("display");

        public static final Attr empty_cells = new Attr("empty-cells");

        public static final Attr float_ = new Attr("float");

        public static final Attr font = new Attr("font");

        public static final Attr font_family = new Attr("font-family");

        public static final Attr font_size = new Attr("font-size");

        public static final Attr font_size_adjust = new Attr("font-size-adjust");

        public static final Attr font_stretch = new Attr("font-stretch");

        public static final Attr font_style = new Attr("font-style");

        public static final Attr font_variant = new Attr("font-variant");

        public static final Attr font_weight = new Attr("font-weight");

        public static final Attr height = new Attr("height");

        public static final Attr left = new Attr("left");

        public static final Attr letter_spacing = new Attr("letter-spacing");

        public static final Attr line_height = new Attr("line-height");

        public static final Attr list_style = new Attr("list-style");

        public static final Attr list_style_image = new Attr("list-style-image");

        public static final Attr list_style_position = new Attr("list-style-position");

        public static final Attr list_style_type = new Attr("list-style-type");

        public static final Attr margin = new Attr("margin");

        public static final Attr margin_bottom = new Attr("margin-bottom");

        public static final Attr margin_left = new Attr("margin-left");

        public static final Attr margin_right = new Attr("margin-right");

        public static final Attr margin_top = new Attr("margin-top");

        public static final Attr marker_offset = new Attr("marker-offset");

        public static final Attr max_height = new Attr("max-height");

        public static final Attr max_width = new Attr("max-width");

        public static final Attr min_height = new Attr("min-height");

        public static final Attr min_width = new Attr("min-width");

        public static final Attr outline = new Attr("outline");

        public static final Attr outline_color = new Attr("outline-color");

        public static final Attr outline_style = new Attr("outline-style");

        public static final Attr outline_width = new Attr("outline-width");

        public static final Attr overflow = new Attr("overflow");

        public static final Attr padding = new Attr("padding");

        public static final Attr padding_bottom = new Attr("padding-bottom");

        public static final Attr padding_left = new Attr("padding-left");

        public static final Attr padding_right = new Attr("padding-right");

        public static final Attr padding_top = new Attr("padding-top");

        public static final Attr position = new Attr("position");

        public static final Attr quotes = new Attr("quotes");

        public static final Attr right = new Attr("right");

        public static final Attr table_layout = new Attr("table-layout");

        public static final Attr text_align = new Attr("text-align");

        public static final Attr text_decoration = new Attr("text-decoration");

        public static final Attr text_indent = new Attr("text-indent");

        public static final Attr text_shadow = new Attr("text-shadow");

        public static final Attr text_transform = new Attr("text-transform");

        public static final Attr top = new Attr("top");

        public static final Attr unicode_bidi = new Attr("unicode-bidi");

        public static final Attr vertical_align = new Attr("vertical-align");

        public static final Attr visibility = new Attr("visibility");

        public static final Attr white_space = new Attr("white-space");

        public static final Attr width = new Attr("width");

        public static final Attr word_spacing = new Attr("word-spacing");

        public static final Attr z_index = new Attr("z-index");
        static
        {
            _cssAttrs.put(background.toString(), background);
            _cssAttrs.put(background_attachment.toString(), background_attachment);
            _cssAttrs.put(background_color.toString(), background_color);
            _cssAttrs.put(background_image.toString(), background_image);
            _cssAttrs.put(background_position.toString(), background_position);
            _cssAttrs.put(background_repeat.toString(), background_repeat);
            _cssAttrs.put(border.toString(), border);
            _cssAttrs.put(border_bottom.toString(), border_bottom);
            _cssAttrs.put(border_bottom_color.toString(), border_bottom_color);
            _cssAttrs.put(border_bottom_style.toString(), border_bottom_style);
            _cssAttrs.put(border_bottom_width.toString(), border_bottom_width);
            _cssAttrs.put(border_collapse.toString(), border_collapse);
            _cssAttrs.put(border_color.toString(), border_color);
            _cssAttrs.put(border_left.toString(), border_left);
            _cssAttrs.put(border_left_color.toString(), border_left_color);
            _cssAttrs.put(border_left_style.toString(), border_left_style);
            _cssAttrs.put(border_left_width.toString(), border_left_width);
            _cssAttrs.put(border_right.toString(), border_right);
            _cssAttrs.put(border_right_color.toString(), border_right_color);
            _cssAttrs.put(border_right_style.toString(), border_right_style);
            _cssAttrs.put(border_right_width.toString(), border_right_width);
            _cssAttrs.put(border_spacing.toString(), border_spacing);
            _cssAttrs.put(border_style.toString(), border_style);
            _cssAttrs.put(border_top.toString(), border_top);
            _cssAttrs.put(border_top_color.toString(), border_top_color);
            _cssAttrs.put(border_top_style.toString(), border_top_style);
            _cssAttrs.put(border_top_width.toString(), border_top_width);
            _cssAttrs.put(border_width.toString(), border_width);
            _cssAttrs.put(bottom.toString(), bottom);
            _cssAttrs.put(caption_side.toString(), caption_side);
            _cssAttrs.put(clear.toString(), clear);
            _cssAttrs.put(clip.toString(), clip);
            _cssAttrs.put(color.toString(), color);
            _cssAttrs.put(content.toString(), content);
            _cssAttrs.put(counter_increment.toString(), counter_increment);
            _cssAttrs.put(counter_reset.toString(), counter_reset);
            _cssAttrs.put(cursor.toString(), cursor);
            _cssAttrs.put(direction.toString(), direction);
            _cssAttrs.put(display.toString(), display);
            _cssAttrs.put(empty_cells.toString(), empty_cells);
            _cssAttrs.put(float_.toString(), float_);
            _cssAttrs.put(font.toString(), font);
            _cssAttrs.put(font_family.toString(), font_family);
            _cssAttrs.put(font_size.toString(), font_size);
            _cssAttrs.put(font_size_adjust.toString(), font_size_adjust);
            _cssAttrs.put(font_stretch.toString(), font_stretch);
            _cssAttrs.put(font_style.toString(), font_style);
            _cssAttrs.put(font_variant.toString(), font_variant);
            _cssAttrs.put(font_weight.toString(), font_weight);
            _cssAttrs.put(height.toString(), height);
            _cssAttrs.put(left.toString(), left);
            _cssAttrs.put(letter_spacing.toString(), letter_spacing);
            _cssAttrs.put(line_height.toString(), line_height);
            _cssAttrs.put(list_style.toString(), list_style);
            _cssAttrs.put(list_style_image.toString(), list_style_image);
            _cssAttrs.put(list_style_position.toString(), list_style_position);
            _cssAttrs.put(list_style_type.toString(), list_style_type);
            _cssAttrs.put(margin.toString(), margin);
            _cssAttrs.put(margin_bottom.toString(), margin_bottom);
            _cssAttrs.put(margin_left.toString(), margin_left);
            _cssAttrs.put(margin_right.toString(), margin_right);
            _cssAttrs.put(margin_top.toString(), margin_top);
            _cssAttrs.put(marker_offset.toString(), marker_offset);
            _cssAttrs.put(max_height.toString(), max_height);
            _cssAttrs.put(max_width.toString(), max_width);
            _cssAttrs.put(min_height.toString(), min_height);
            _cssAttrs.put(min_width.toString(), min_width);
            _cssAttrs.put(outline.toString(), outline);
            _cssAttrs.put(outline_color.toString(), outline_color);
            _cssAttrs.put(outline_style.toString(), outline_style);
            _cssAttrs.put(outline_width.toString(), outline_width);
            _cssAttrs.put(overflow.toString(), overflow);
            _cssAttrs.put(padding.toString(), padding);
            _cssAttrs.put(padding_bottom.toString(), padding_bottom);
            _cssAttrs.put(padding_left.toString(), padding_left);
            _cssAttrs.put(padding_right.toString(), padding_right);
            _cssAttrs.put(padding_top.toString(), padding_top);
            _cssAttrs.put(position.toString(), position);
            _cssAttrs.put(quotes.toString(), quotes);
            _cssAttrs.put(right.toString(), right);
            _cssAttrs.put(table_layout.toString(), table_layout);
            _cssAttrs.put(text_align.toString(), text_align);
            _cssAttrs.put(text_decoration.toString(), text_decoration);
            _cssAttrs.put(text_indent.toString(), text_indent);
            _cssAttrs.put(text_shadow.toString(), text_shadow);
            _cssAttrs.put(text_transform.toString(), text_transform);
            _cssAttrs.put(top.toString(), top);
            _cssAttrs.put(unicode_bidi.toString(), unicode_bidi);
            _cssAttrs.put(vertical_align.toString(), vertical_align);
            _cssAttrs.put(visibility.toString(), visibility);
            _cssAttrs.put(white_space.toString(), white_space);
            _cssAttrs.put(width.toString(), width);
            _cssAttrs.put(word_spacing.toString(), word_spacing);
            _cssAttrs.put(z_index.toString(), z_index);
        }

        /**
         * Resolve the str to an attr representation
         * @param str the str
         * @return the Attribute
         */
        public static Attr resolveAttr(String str)
        {
            return (Attr) _cssAttrs.get(str);
        }

        /**
         * Return all the defined css attributes
         * @return all the defined css attribute objects
         */
        public static List getAllAttrs()
        {
            return new ArrayList(_cssAttrs.values());
        }

        /**
         * Constructor
         * @param name the name of the attribute
         */
        private Attr(String name)
        {
            super(name);
        }
    }

    /**
     * The css field definitions
     * @author TiongHiang Lee (thlee@onemindsoft.org)
     * @version $Id: Css.java,v 1.3 2005/03/07 17:28:17 thlee Exp $ $Name:  $
     */
    public static final class AttrUnit extends Enum
    {

        public static final AttrUnit deg = new AttrUnit("deg");

        public static final AttrUnit grad = new AttrUnit("grad");

        public static final AttrUnit hz = new AttrUnit("Hz");

        public static final AttrUnit khz = new AttrUnit("kHz");

        public static final AttrUnit ms = new AttrUnit("ms");

        public static final AttrUnit percent = new AttrUnit("%");

        public static final AttrUnit point = new AttrUnit("pt");

        public static final AttrUnit rad = new AttrUnit("rad");

        public static final AttrUnit s = new AttrUnit("s");

        /**
         * {@inheritDoc}
         */
        private AttrUnit(String name)
        {
            super(name);
        }
    }

    /**
     * The offset value
     * @author TiongHiang Lee (thlee@onemindsoft.org)
     * @version $Id: Css.java,v 1.3 2005/03/07 17:28:17 thlee Exp $ $Name:  $
     */
    public static class AttrValue extends Enum
    {

        public static final AttrValue absolute = new AttrValue("absolute");

        public static final AttrValue armenian = new AttrValue("armenian");

        public static final AttrValue auto = new AttrValue("auto");

        public static final AttrValue baseline = new AttrValue("baseline");

        public static final AttrValue bidi_override = new AttrValue("bidi-override");

        public static final AttrValue block = new AttrValue("block");

        public static final AttrValue bold = new AttrValue("bold");

        public static final AttrValue bolder = new AttrValue("bolder");

        public static final AttrValue both = new AttrValue("both");

        public static final AttrValue bottom = new AttrValue("bottom");

        public static final AttrValue capitalize = new AttrValue("capitalize");

        public static final AttrValue caption = new AttrValue("caption");

        public static final AttrValue center = new AttrValue("center");

        public static final AttrValue circle = new AttrValue("circle");

        public static final AttrValue cjk_ideographic = new AttrValue("cjk-ideographic");

        public static final AttrValue close_quote = new AttrValue("close-quote");

        public static final AttrValue collapse = new AttrValue("collapse");

        public static final AttrValue compact = new AttrValue("compact");

        public static final AttrValue condensed = new AttrValue("condensed");

        public static final AttrValue crosshair = new AttrValue("crosshair");

        public static final AttrValue decimal = new AttrValue("decimal");

        public static final AttrValue decimal_leading_zero = new AttrValue("decimal-leading-zero");

        public static final AttrValue default_ = new AttrValue("default");

        public static final AttrValue disc = new AttrValue("disc");

        public static final AttrValue e_resize = new AttrValue("e-resize");

        public static final AttrValue embed = new AttrValue("embed");

        public static final AttrValue expanded = new AttrValue("expanded");

        public static final AttrValue extra_condensed = new AttrValue("extra-condensed");

        public static final AttrValue extra_expanded = new AttrValue("extra-expanded");

        public static final AttrValue fixed = new AttrValue("fixed");

        public static final AttrValue georgian = new AttrValue("georgian");

        public static final AttrValue hebrew = new AttrValue("hebrew");

        public static final AttrValue help = new AttrValue("help");

        public static final AttrValue hidden = new AttrValue("hidden");

        public static final AttrValue hide = new AttrValue("hide");

        public static final AttrValue hiragana = new AttrValue("hiragana");

        public static final AttrValue hiragana_iroha = new AttrValue("hiragana-iroha");

        public static final AttrValue icon = new AttrValue("icon");

        public static final AttrValue inherit = new AttrValue("inherit");

        public static final AttrValue inline = new AttrValue("inline");

        public static final AttrValue inline_table = new AttrValue("inline-table");

        public static final AttrValue inside = new AttrValue("inside");

        public static final AttrValue invert = new AttrValue("invert");

        public static final AttrValue italic = new AttrValue("italic");

        public static final AttrValue justify = new AttrValue("justify");

        public static final AttrValue katakana = new AttrValue("katakana");

        public static final AttrValue katakana_iroha = new AttrValue("katakana-iroha");

        public static final AttrValue left = new AttrValue("left");

        public static final AttrValue lighter = new AttrValue("lighter");

        public static final AttrValue list_item = new AttrValue("list-item");

        public static final AttrValue lower_alpha = new AttrValue("lower-alpha");

        public static final AttrValue lower_greek = new AttrValue("lower-greek");

        public static final AttrValue lower_latin = new AttrValue("lower-latin");

        public static final AttrValue lower_roman = new AttrValue("lower-roman");

        public static final AttrValue lowercase = new AttrValue("lowercase");

        public static final AttrValue ltr = new AttrValue("ltr");

        public static final AttrValue marker = new AttrValue("marker");

        public static final AttrValue menu = new AttrValue("menu");

        public static final AttrValue message_box = new AttrValue("message-box");

        public static final AttrValue middle = new AttrValue("middle");

        public static final AttrValue move = new AttrValue("move");

        public static final AttrValue n_resize = new AttrValue("n-resize");

        public static final AttrValue narrower = new AttrValue("narrower");

        public static final AttrValue ne_resize = new AttrValue("ne-resize");

        public static final AttrValue no_open_quote = new AttrValue("no-open-quote");

        public static final AttrValue no_repeat = new AttrValue("no-repeat");

        public static final AttrValue none = new AttrValue("none");

        public static final AttrValue normal = new AttrValue("normal");

        public static final AttrValue nowrap = new AttrValue("nowrap");

        public static final AttrValue nw_resize = new AttrValue("nw-resize");

        public static final AttrValue oblique = new AttrValue("oblique");

        public static final AttrValue open_quote = new AttrValue("open-quote");

        public static final AttrValue outside = new AttrValue("outside");

        public static final AttrValue pointer = new AttrValue("pointer");

        public static final AttrValue pre = new AttrValue("pre");

        public static final AttrValue relative = new AttrValue("relative");

        public static final AttrValue repeat = new AttrValue("repeat");

        public static final AttrValue repeat_x = new AttrValue("repeat-x");

        public static final AttrValue repeat_y = new AttrValue("repeat-y");

        public static final AttrValue right = new AttrValue("right");

        public static final AttrValue rtl = new AttrValue("rtl");

        public static final AttrValue run_in = new AttrValue("rul-in");

        public static final AttrValue s_resize = new AttrValue("s-resize");

        public static final AttrValue scroll = new AttrValue("scroll");

        public static final AttrValue se_resize = new AttrValue("se-resize");

        public static final AttrValue semi_condensed = new AttrValue("semi-condensed");

        public static final AttrValue semi_expanded = new AttrValue("semi-expanded");

        public static final AttrValue separate = new AttrValue("separate");

        public static final AttrValue show = new AttrValue("show");

        public static final AttrValue small_caps = new AttrValue("small-caps");

        public static final AttrValue small_caption = new AttrValue("small-caption");

        public static final AttrValue square = new AttrValue("square");

        public static final AttrValue static_ = new AttrValue("static");

        public static final AttrValue status_bar = new AttrValue("status-bar");

        public static final AttrValue sub = new AttrValue("sub");

        public static final AttrValue super_ = new AttrValue("super");

        public static final AttrValue sw_resize = new AttrValue("sw-resize");

        public static final AttrValue table = new AttrValue("table");

        public static final AttrValue table_caption = new AttrValue("table-caption");

        public static final AttrValue table_cell = new AttrValue("table-cell");

        public static final AttrValue table_column = new AttrValue("table-column");

        public static final AttrValue table_column_group = new AttrValue("table-column-group");

        public static final AttrValue table_footer_group = new AttrValue("table-footer-group");

        public static final AttrValue table_header_group = new AttrValue("table-header-group");

        public static final AttrValue table_row = new AttrValue("table-row");

        public static final AttrValue table_row_group = new AttrValue("table-row-group");

        public static final AttrValue text = new AttrValue("text");

        public static final AttrValue text_bottom = new AttrValue("text-bottom");

        public static final AttrValue text_top = new AttrValue("text-top");

        public static final AttrValue top = new AttrValue("top");

        public static final AttrValue transparent = new AttrValue("transparent");

        public static final AttrValue ultra_condensed = new AttrValue("ultra-condensed");

        public static final AttrValue ultra_expanded = new AttrValue("ultra-expanded");

        public static final AttrValue upper_alpha = new AttrValue("upper-alpha");

        public static final AttrValue upper_latin = new AttrValue("upper-latin");

        public static final AttrValue upper_roman = new AttrValue("upper-roman");

        public static final AttrValue uppercase = new AttrValue("uppercase");

        public static final AttrValue visible = new AttrValue("visible");

        public static final AttrValue w_resize = new AttrValue("w-resize");

        public static final AttrValue wait = new AttrValue("wait");

        public static final AttrValue wider = new AttrValue("wider");

        /**
         * {@inheritDoc}
         */
        private AttrValue(String name)
        {
            super(name);
        }
    }
}