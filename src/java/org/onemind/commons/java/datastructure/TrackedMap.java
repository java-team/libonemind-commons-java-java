/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.*;
/**
 * A map that has it's map values changes tracked. It uses an inner map to keep the unchanged value and itself to track the changes
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: TrackedMap.java,v 1.2 2004/08/26 12:33:16 thlee Exp $ $Name:  $
 */
public class TrackedMap extends HashMap
{

    /** the track inner map * */
    private Map _tracked;

    /**
     * {@inheritDoc}
     */
    public TrackedMap()
    {
        this(new HashMap());
    }

    /**
     * {@inheritDoc}
     */
    public TrackedMap(int initialCapacity)
    {
        super(initialCapacity);
    }

    /**
     * {@inheritDoc}
     */
    public TrackedMap(int initialCapacity, float loadFactor)
    {
        super(initialCapacity, loadFactor);
    }

    /**
     * {@inheritDoc}
     */
    public TrackedMap(Map map)
    {
        super();
        _tracked = map;
    }

    /**
     * Return whether this map has been changes
     * @return true if it has been changed
     */
    public boolean hasChanges()
    {
        return getChangedKeySet().size() > 0;
    }

    /**
     * Return the key set of changed values
     * @return the key set
     */
    public Set getChangedKeySet()
    {
        return super.keySet();
    }

    /**
     * {@inheritDoc}
     */
    public Object get(Object key)
    {
        if (containsKey(key))
        {
            return super.get(key);
        } else
        {
            return _tracked.get(key);
        }
    }

    /**
     * Make this map as up-to-date.
     */
    public void makeUpToDate()
    {
        Iterator it = super.keySet().iterator();
        while (it.hasNext())
        {
            Object key = it.next();
            Object o = super.get(key);
            _tracked.put(key, o);
        }
        super.clear();
    }

    /**
     * Clear all the changes
     */
    public void clearChanges()
    {
        super.clear();
    }

    /**
     * {@inheritDoc}
     */
    public void clear()
    {
        super.clear();
        _tracked.clear();
    }

    /**
     * {@inheritDoc}
     */
    public boolean containsKey(Object key)
    {
        return super.containsKey(key) || _tracked.containsKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public boolean containsValue(Object value)
    {
        return super.containsValue(value) || _tracked.containsValue(value);
    }

    /**
     * {@inheritDoc}
     */
    public Set entrySet()
    {
        Set s = new HashSet(_tracked.entrySet());
        s.addAll(super.entrySet());
        return s;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isEmpty()
    {
        return super.isEmpty() && _tracked.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    public Set keySet()
    {
        Set s = new HashSet(_tracked.keySet());
        s.addAll(super.keySet());
        return s;
    }

    /**
     * {@inheritDoc}
     */
    public Object remove(Object key)
    {
        Object o = get(key);
        put(key, null);
        return o;
    }

    /**
     * {@inheritDoc}
     */
    public int size()
    {
        return keySet().size();
    }

    /**
     * {@inheritDoc}
     */
    public Collection values()
    {
        HashSet set = new HashSet(_tracked.values());
        set.addAll(super.values());
        return set;
    }
}