/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

/**
 * A stack associated with current thread
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: ThreadLocalStack.java,v 1.2 2004/08/26 12:33:16 thlee Exp $ $Name:  $
 */
public class ThreadLocalStack
{

    /** the thread local * */
    private ThreadLocal _local = new ThreadLocal();

    /**
     * Push a local object the the thread local stack
     * @param localObject the local object
     * @return the size after the push
     */
    public int pushLocal(Object localObject)
    {
        return getLocalStack().pushReturnSize(localObject);
    }

    /**
     * get the local stack
     * @return the stack
     */
    public Stack getLocalStack()
    {
        Stack s = (Stack) _local.get();
        if (s == null)
        {
            s = new Stack();
            _local.set(s);
        }
        return s;
    }

    /**
     * Get the top-most local object in local stack
     * @return the top-most local object in local stack
     */
    public Object getLocal()
    {
        return getLocalStack().peek();
    }

    /**
     * Pop uptil certain size in local stack
     * @param i the size
     */
    public void popLocalUtil(int i)
    {
        getLocalStack().popUntil(i);
    }

    /**
     * Pop the top-most local object in threadlocal stack
     * @return the top-most local object
     */
    public Object popLocal()
    {
        return getLocalStack().pop();
    }
}