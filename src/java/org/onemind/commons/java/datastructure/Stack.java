/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

/**
 * An extension of java.util.Stack to have popUntil and pushReturnSize method
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: Stack.java,v 1.2 2004/08/26 12:33:16 thlee Exp $ $Name:  $
 */
public class Stack extends java.util.Stack
{

    /**
     * {@inheritDoc}
     */
    public Stack()
    {
    }

    /**
     * Push the object ot the stack and return the size before pushing the object in
     * @param o the object
     * @return the size before the push
     */
    public int pushReturnSize(Object o)
    {
        int i = size();
        push(o);
        return i;
    }

    /**
     * Pop until the stack reach size i
     * @param i the size
     */
    public void popUntil(int i)
    {
        if (i < 0)
        {
            throw new IllegalArgumentException("Invalid size for popUtil");
        } else if (i > size())
        {
            throw new RuntimeException("Stack already less than " + i);
        }
        while (size() > i)
        {
            pop();
        }
    }
}