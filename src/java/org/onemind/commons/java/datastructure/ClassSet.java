/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.*;
/**
 * Represents a set of classes. User can use isSubSetOf() to detect whether a given class is subclass of a class in the class set
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: ClassSet.java,v 1.3 2004/08/26 12:33:16 thlee Exp $ $Name:  $
 */
public class ClassSet
{

    /** the classes * */
    private HashSet _classes = new HashSet();

    /**
     * {@inheritDoc}
     */
    public ClassSet()
    {
        super();
    }

    /**
     * {@inheritDoc}
     */
    public ClassSet(Collection c)
    {
        addAll(c);
    }

    /**
     * Add all in the classes to the ClassSet
     * @param classes the collection containing the classes
     */
    public void addAll(Collection classes)
    {
        Iterator it = classes.iterator();
        while (it.hasNext())
        {
            Object o = it.next();
            if (o instanceof Class)
            {
                add((Class) o);
            } else
            {
                throw new IllegalArgumentException(o + " is not a subclass of class");
            }
        }
    }

    /**
     * Add the class 
     * @param c the class
     */
    public void add(Class c)
    {
        _classes.add(c);
    }

    /**
     * Check whether the class is subclass of one of the class in the class set
     * @param c the class
     * @return true if is subclass
     */
    public boolean isSubclassOfClasses(Class c)
    {
        Class current = c;
        while ((current != null) && (current != Object.class))
        {
            if (_classes.contains(c))
            {
                return true;
            } else
            {
                current = current.getSuperclass();
            }
        }
        Class[] interfaces = c.getInterfaces();
        for (int i = 0; i < interfaces.length; i++)
        {
            if (_classes.contains(interfaces[i]))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the classes
     * @return the classes
     */
    public Set getClasses()
    {
        return _classes;
    }
}