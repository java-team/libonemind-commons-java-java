/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import java.util.Map;

/**
 * A name table interface
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public interface Nametable
{

    /**
     * Declare a variable in the name table
     * @param name the name
     * @param value the value
     */
    void declare(String name, Object value);

    /**
     * Assign a variable in the name table
     * @param name the name
     * @param value the value
     * @return the old value, or null
     */
    Object assign(String name, Object value);

    /**
     * Whether the nametable contains the name
     * @param name the name
     * @return true if contains the name
     */
    boolean containsName(String name);
    
    /**
     * Access the value associated with name
     * @param name
     * @return
     */
    Object access(String name);
    
    /**
     * Undeclare the name
     * @param name
     */
    void undeclare(String name);
    
    /**
     * Return map representation of this nametable
     * @return unmodifiable map representation of this nametable
     */
    Map asMap();
}
