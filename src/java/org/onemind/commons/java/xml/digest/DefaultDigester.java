/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@thinklient.org
 */

package org.onemind.commons.java.xml.digest;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
/**
 * A default digesters that does nothing
 * @author TiongHiang Lee (thlee@thinklient.org)
 */
public class DefaultDigester implements ElementDigester
{

    /** the element name **/
    private final String _elementName;

    /**
     * Constructor
     * @param name the name
     */
    public DefaultDigester(String name)
    {
        _elementName = name;
    }    

    /**
     * {@inheritDoc}
     */
    public void startDigest(SaxDigesterHandler handler, Attributes attrs) throws SAXException
    {
        //do nothing
    }

    /**
     * {@inheritDoc}
     */
    public void endDigest(SaxDigesterHandler handler) throws SAXException
    {
        //do nothing
    }

    /**
     * {@inheritDoc}
     */
    public void characters(SaxDigesterHandler handler, char[] chars, int offset, int length) throws SAXException
    {
        // do nothing
    }

    /** 
     * {@inheritDoc}
     */
    public final String getElementName()
    {
        return _elementName;
    }
}