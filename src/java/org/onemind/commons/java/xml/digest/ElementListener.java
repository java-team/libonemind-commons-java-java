/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.xml.digest;

import org.onemind.commons.java.event.EventListener;

/**
 * The object creation listener
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: ElementListener.java,v 1.1 2005/01/30 06:30:53 thlee Exp $ $Name:  $
 */
public interface ElementListener extends EventListener
{
    /**
     * Called by an ElementCreatorDigester when an element is created
     * @param obj the object created
     */
    public void objectCreated(Object obj);
}