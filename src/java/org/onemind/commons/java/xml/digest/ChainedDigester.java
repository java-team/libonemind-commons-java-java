/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.xml.digest;

import org.onemind.commons.java.lang.reflect.ReflectUtils;
import org.onemind.commons.java.util.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
/**
 * A ChainedDigester is a helper digester that chains the digestion
 * of xml dynamically based on a dynamic digester configured as a 
 * attribute name. 
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public class ChainedDigester extends AbstractElementCreatorDigester
{
    /** the attribute name that specify the digester class **/
    private String _attrName;

    /** the argument to pass the constructor of the dynamic digester **/
    private Object[] _args;

    /**
     * Constructor
     * @param name the name of element
     */
    public ChainedDigester(String name)
    {
        this(name, "className", null);
    }

    /**
     * Constructor
     * @param name the element name
     * @param attrName the attr
     */
    public ChainedDigester(String name, String attrName)
    {
        this(name, attrName, null);
    }

    /**
     * Constructor
     * @param name the element name
     * @param attrName the attribute the specifies the dynamic digester
     * @param args arguments to pass to constructor of the dynamic digester
     */
    public ChainedDigester(String name, String attrName, Object[] args)
    {
        super(name);
        _attrName = attrName;
        _args = args;
    }

    /** 
     * {@inheritDoc}
     */
    public void startDigest(SaxDigesterHandler handler, Attributes attrs) throws SAXException
    {
        String className = attrs.getValue(_attrName);
        if (StringUtils.isNullOrEmpty(className))
        {
            throw new SAXException("className attribute need to be present at " + handler.getCurrentPath());
        } else
        {
            try
            {
                ElementDigester dig = (ElementDigester) ReflectUtils.newInstance(ReflectUtils.getClass(className), _args);
                setCreatedElement(dig);
                handler.addSubDigester(dig);
            } catch (Exception e)
            {
                throw new SAXException("Cannot instantiate render context " + className, e);
            }
        }
    }
}