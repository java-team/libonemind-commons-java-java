/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.lang;

/**
 * An mutable long
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: MutableLong.java,v 1.3 2004/09/03 13:24:14 thlee Exp $ $Name:  $
 */
public class MutableLong extends Number
{

    /** the long value * */
    private long _value;

    /**
     * {@inheritDoc}
     * @param l the long value
     */
    public MutableLong(long l)
    {
        _value = l;
    }

    /**
     * Set the long value
     * @param l the long value
     */
    public final void set(long l)
    {
        _value = l;
    }

    /**
     * {@inheritDoc}
     */
    public final byte byteValue()
    {
        return (byte) _value;
    }

    /**
     * {@inheritDoc}
     */
    public final double doubleValue()
    {
        return (double) _value;
    }

    /**
     * {@inheritDoc}
     */
    public final float floatValue()
    {
        return (float) _value;
    }

    /**
     * {@inheritDoc}
     */
    public final int intValue()
    {
        return (int) _value;
    }

    /**
     * {@inheritDoc}
     */
    public final long longValue()
    {
        return _value;
    }

    /**
     * {@inheritDoc}
     */
    public final short shortValue()
    {
        return (short) _value;
    }
    
    /**
     * increase by i
     * @param i the value to increase
     */
    public void inc(long i)
    {
        _value += i;
    }
    
    /**
     * decrease by i
     * @param i the value to decrease
     */
    public void dec(long i)
    {
        _value -= i;
    }
    
    /** 
     * {@inheritDoc}
     */
    public String toString()
    {
        return String.valueOf(_value);
    }
}