/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@thinklient.org
 */

package org.onemind.commons.java.xml.digest;

import java.io.InputStream;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import junit.framework.TestCase;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
/**
 * SaxDigester test. The model is with exactly one "RootElement" which can have 0-1 "Element" that can any random "SubElement1" and
 * "SubElement2"
 * @author TiongHiang Lee (thlee@thinklient.org)
 * @version $Id: SaxDigesterTest.java,v 1.4 2005/04/26 17:46:39 thlee Exp $ $Name:  $
 */
public class SaxDigesterTest extends TestCase
{

    /**
     * Root element digester
     */
    public class TestElementDigester extends DefaultDigester
    {

        /** the count start is called**/
        int startCount;

        /** the count end is called **/
        int endCount;

        /** the count characters is called **/
        int charCount;

        /**
         * {@inheritDoc}
         */
        public TestElementDigester(String str)
        {
            super(str);
        }

        /** 
         * {@inheritDoc}
         */
        public void startDigest(SaxDigesterHandler handler, Attributes attrs) throws SAXException
        {
            startCount++;
            //System.out.println("Starting " + handler.getCurrentPath());
        }

        /** 
         * {@inheritDoc}
         */
        public void endDigest(SaxDigesterHandler handler) throws SAXException
        {
            endCount++;
            //System.out.println("Ending " + handler.getCurrentPath());
        }

        /** 
         * {@inheritDoc}
         */
        public void characters(SaxDigesterHandler handler, char[] chars, int offset, int length) throws SAXException
        {
            charCount++;
            //System.out.println("Character " + handler.getCurrentPath());            
        }
    }

    /**
     * parse the file with the handler 
     * @throws Exception
     */
    private void _testHandler(InputStream in, SaxDigesterHandler handler) throws Exception
    {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        // Parse the input
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(in, handler);
    }

    /**
     * Test when there's one element
     * @throws Exception if there's exception
     */
    public void testOneElement() throws Exception
    {
        SaxDigesterHandler handler = new SaxDigesterHandler();
        TestElementDigester rootElement = new TestElementDigester("RootElement");
        TestElementDigester rootElement1 = new TestElementDigester("RootElment1");
        TestElementDigester subElement1 = new TestElementDigester("SubElement1");
        handler.addDigester(rootElement);
        handler.addDigester(rootElement1);
        handler.addDigester("RootElement/Element1", subElement1);
        _testHandler(getClass().getResourceAsStream("SaxDigesterTest.xml"), handler);
        assertTrue(rootElement.startCount == 1);
        assertTrue(rootElement.endCount == 1);          
        assertTrue(rootElement1.startCount == 0);
        assertTrue(rootElement1.endCount == 0);        
        assertTrue(subElement1.startCount == 2);
        assertTrue(subElement1.endCount == 2);
    }
}