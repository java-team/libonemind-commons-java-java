/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import org.onemind.commons.java.datastructure.NametableStack;
import junit.framework.TestCase;
/**
 * Test for nametable stack
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: NametableStackTest.java,v 1.3 2005/04/06 15:40:43 thlee Exp $ $Name:  $
 */
public class NametableStackTest extends TestCase
{

    public void testCloseScope()
    {
        NametableStack stack = new NametableStack();
        //add 0-4
        for (int i = 0; i < 5; i++)
        {
            String name = String.valueOf(i);
            stack.declare(name, name);
        }
        //open new scope
        int scope = stack.newScope();
        //add 5-9
        for (int i = 5; i < 10; i++)
        {
            String name = String.valueOf(i);
            stack.declare(name, name);
        }
        // check all values valid
        for (int i = 0; i < 10; i++)
        {
            String name = String.valueOf(i);
            assertEquals(stack.access(name), name);
        }
        //close scope
        stack.closeScope(scope);
        //check only  0-4 valid
        for (int i = 5; i < 10; i++)
        {
            String name = String.valueOf(i);
            assertFalse(stack.containsName(name));
        }
        for (int i = 0; i < 5; i++)
        {
            String name = String.valueOf(i);
            assertEquals(stack.access(name), name);
        }
    }

    public void testLocalScope()
    {
        NametableStack stack = new NametableStack();
        //add 0-4
        for (int i = 0; i < 5; i++)
        {
            String name = String.valueOf(i);
            stack.declare(name, name);
        }
        //open new local scope
        int scope = stack.newLocalScope();
        //add 5-9
        for (int i = 2; i < 7; i++)
        {
            String name = String.valueOf(i);
            stack.declare(name, name);
        }
        // check all values valid
        for (int i = 0; i < 7; i++)
        {
            String name = String.valueOf(i);
            assertEquals(stack.access(name), name);
        }
        //close scope
        stack.closeLocalScope(scope);
        //check only  0-4 valid
        for (int i = 5; i < 7; i++)
        {
            String name = String.valueOf(i);
            assertFalse(stack.containsName(name));
        }
        for (int i = 0; i < 5; i++)
        {
            String name = String.valueOf(i);
            assertEquals(stack.access(name), name);
        }
        
        
    }
}