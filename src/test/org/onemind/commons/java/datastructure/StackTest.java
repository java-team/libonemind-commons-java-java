/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.datastructure;

import org.onemind.commons.java.datastructure.Stack;
import junit.framework.TestCase;

/**
 * Test for stack
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: StackTest.java,v 1.2 2004/08/26 12:33:08 thlee Exp $ $Name:  $
 */
public class StackTest extends TestCase
{

    public void testStack()
    {
        Stack st = new Stack();
        assertEquals(st.pushReturnSize(new Integer(1)), 0);
        assertEquals(st.pushReturnSize(new Integer(2)), 1);
        st.popUntil(1);
        assertEquals(st.size(), 1);
        assertTrue(st.contains(new Integer(1)));
    }
}