/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.commons.java.lang.ref;

import junit.framework.TestCase;
/**
 * Testing SoftHashMap
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * @version $Id: SoftHashMapTest.java,v 1.2 2004/10/31 16:03:13 thlee Exp $ $Name:  $
 */
public class SoftHashMapTest extends TestCase
{

    /** the map **/
    private SoftHashMap _map;

    /** 
     * {@inheritDoc}
     */
    public void setUp()
    {
        _map = new SoftHashMap();
    }

    /**
     * Test the map
     * @throws Exception  
     */
    public void testMap() throws Exception
    {
        boolean collected = false;
        for (int i = 1; i < 1000; i++)
        {
            _map.put(new Integer(i), new Integer(i));
            if (_map.size() < i)
            {
                collected = true;
                break;
            }
            System.gc();
        }
        if (!collected)
        {
            throw new Exception("SoftHashMap entries are not garbage collected");
        }
    }
}